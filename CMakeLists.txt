include(FetchContent)

cmake_minimum_required(VERSION 3.13)

project(two-phase-validation VERSION 1.0
    DESCRIPTION "A library of models and function-objects used for validating the motion of a fluid-interface computed by Direct Numerical Simulation (DNS) methods for two-phase flow implemented in OpenFOAM. "
        LANGUAGES CXX)
enable_testing()

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# Keep the installation local for now (TT).
set(CMAKE_INSTALL_PREFIX "./")

# Use lib instead of lib64 for libraries.
set(CMAKE_INSTALL_LIBDIR "lib")

# Set the build type suffix based on the build type.
set(PROJECT_BUILD_TYPE "")
if ("${CMAKE_BUILD_TYPE}" STREQUAL "Release")
    message ("Using 'Release' CMAKE_BUILD_TYPE.")
elseif ("${CMAKE_BUILD_TYPE}" STREQUAL "Debug")
    message ("Using 'Debug' CMAKE_BUILD_TYPE.")
    set(PROJECT_BUILD_TYPE "Debug")
elseif ("${CMAKE_BUILD_TYPE}" STREQUAL "RelWithDebInfo")
    message ("Using 'RelWithDebInfo' CMAKE_BUILD_TYPE.")
    set(PROJECT_BUILD_TYPE "RelWithDebInfo")
else()
    message (FATAL_ERROR "Use '-DCMAKE_BUILD_TYPE=' to define a build type. 'Release', 'Debug' and 'RelWithDebInfo' are supported.")
    return(1)
endif()

#------------------------------------------------------------------------------
#   Dependencies
#------------------------------------------------------------------------------

# OpenFOAM
# Read build relevant OpenFOAM environment variables and cache them
set(of_wm_project_dir "$ENV{WM_PROJECT_DIR}" CACHE PATH "Path to OpenFOAM project folder.")
set(of_wm_arch "$ENV{WM_ARCH}" CACHE STRING "Architecture. Usually linux64.")
set(of_wm_arch_option "$ENV{WM_ARCH_OPTION}" CACHE STRING "Information if 32 or 64 bit operating system.")
set(of_wm_precision_option "$ENV{WM_PRECISION_OPTION}" CACHE STRING "Flag if to use single precision (SP) or double precision (DP).")
set(of_wm_label_size "$ENV{WM_LABEL_SIZE}" CACHE STRING "Size in bit to use as label type. Can be either 32 or 64.")
set(of_wm_compile_option "$ENV{WM_COMPILE_OPTION}" CACHE STRING "OpenFOAM build type: Opt, Debug, Prof.")
set(of_wm_compiler "$ENV{WM_COMPILER}" CACHE STRING "Compiler used for OpenFOAM build.")
set(of_wm_label_option "$ENV{WM_LABEL_OPTION}" CACHE STRING "Concrete Type used for label. Either Int32 or Int64.")
# WM_ARCH + WM_COMPILER + WM_PRECISION_OPTION + WM_LABEL_OPTION + WM_COMPILE_OPTION
set(of_wm_options "${of_wm_arch}${of_wm_compiler}${of_wm_precision_option}${of_wm_label_option}" CACHE STRING "Name of subfolder which contains compiled OpenFOAM libraries" FORCE)

# Determine whether specific build exists
# TODO: change build path according to build option passed to cmake command (TT)
if(IS_DIRECTORY "${of_wm_project_dir}/platforms/${of_wm_options}${of_wm_compile_option}")
    set(of_wm_options "${of_wm_options}${of_wm_compile_option}")
else()
    message(FATAL_ERROR "Path ${of_wm_project_dir}/platforms/${of_wm_options}${of_wm_compile_option} does not exist. Make sure OpenFOAM environment is set.")
endif()

set(of_lib_path "${of_wm_project_dir}/platforms/${of_wm_options}/lib" CACHE PATH "Path to compiled OpenFOAM libraries.")
set(of_src_path "${of_wm_project_dir}/src" CACHE PATH "Path to OpenFOAM/src folder")

# Use lib instead of lib64 for libraries.
set(CMAKE_INSTALL_LIBDIR "$ENV{FOAM_USER_LIBBIN}")
set(CMAKE_INSTALL_BINDIR "$ENV{FOAM_USER_APPBIN}")

message(STATUS "OpenFOAM lib path: ${of_lib_path}")
message(STATUS "OpenFOAM src path: ${of_src_path}")

# Required OpenFOAM libraries
find_library(OF_FINITEVOLUME finiteVolume PATHS ${of_lib_path})
find_library(OF_MESHTOOLS meshTools PATHS ${of_lib_path})
find_library(OF_OPENFOAM OpenFOAM PATHS ${of_lib_path})
find_library(OF_SURFMESH surfMesh PATHS ${of_lib_path})

# OpenFoam depencies of the lent library
set(OF_DEPS ${OF_FINITEVOLUME} ${OF_MESHTOOLS} ${OF_SURFMESH} ${OF_OPENFOAM} dl m)

# TODO:
# Check that those variables are set. Optional: check for reasonable values (TT)

# Compile definitions required for OpenFOAM
add_compile_definitions(
    WM_LABEL_SIZE=${of_wm_label_size}
    WM_${of_wm_precision_option}
    WM_ARCH_OPTION=${of_wm_arch_option}
    ${of_wm_arch}
    OPENFOAM="$ENV{WM_PROJECT_VERSION}" # Figures out OF version. TM. 
    NoRepository
)

# Required to make linking to OpenFOAM libraries work
set(CMAKE_EXE_LINKER_FLAGS "-Xlinker --add-needed -Xlinker --no-as-needed")

if(MSVC)
    add_compile_options(/W4 /WX)
else(MSVC)
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -ggdb3 -O0 -Wall -Wextra -Wconversion -DDEBUG -pedantic -D_USE_MATH_DEFINES -DBOOST_MATH_INSTRUMENT")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O3 -Wall -Wextra -Wconversion -pedantic -Wno-deprecated -D_USE_MATH_DEFINES")
    set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELEASE} -ggdb3 -O3 -Wall -Wextra -Wconversion -pedantic -DDEBUG -D_USE_MATH_DEFINES")
endif(MSVC)



#------------------------------------------------------------------------------
#   Library
#------------------------------------------------------------------------------
add_library("twoPhaseValidation" SHARED
    # NOTE: commented lines give errors due to missing header files (TT)
    src/divFree/divFreeFunctionObject/divFreeFunctionObject.C
    src/divFree/fieldModels/deformation2DFieldModel/deformation2DFieldModel.C
    src/divFree/fieldModels/deformationFieldModel/deformationFieldModel.C
    src/divFree/fieldModels/divFreeFieldModel/divFreeFieldModel.C
    src/divFree/fieldModels/harmonicFieldModel/harmonicFieldModel.C
    src/divFree/fieldModels/periodicFieldModel/periodicFieldModel.C
    src/divFree/fieldModels/rotationFieldModel/rotationFieldModel.C
    src/divFree/fieldModels/shear2DFieldModel/shear2DFieldModel.C
    src/divFree/fieldModels/shearFieldModel/shearFieldModel.C
    src/divFree/fieldModels/supermanFieldModel/supermanFieldModel.C
    src/divFree/fieldModels/translationFieldModel/translationFieldModel.C
    #src/error/fieldErrorBoundedness/fieldErrorsBoundedness.C
    src/error/fieldErrorBoundedness/volScalarFieldErrorBoundedness.C
    src/error/fieldErrorComposite/fieldErrorComposites.C
    src/error/fieldError/fieldErrors.C
    src/error/fieldErrorFunctionObject/fieldErrorFunctionObject.C
    #src/error/fieldErrorInterfaceSharpness2/fieldErrorsInterfaceSharpness2.C
    src/error/fieldErrorInterfaceSharpness2/volScalarFieldErrorInterfaceSharpness2.C
    #src/error/fieldErrorInterfaceSharpness3/fieldErrorsInterfaceSharpness3.C
    src/error/fieldErrorInterfaceSharpness3/volScalarFieldErrorInterfaceSharpness3.C
    #src/error/fieldErrorInterfaceSharpness/fieldErrorsInterfaceSharpness.C
    src/error/fieldErrorInterfaceSharpness/volScalarFieldErrorInterfaceSharpness.C
    src/error/fieldErrorL1/fieldErrorsL1.C
    src/error/fieldErrorL1normalized/fieldErrorsL1normalized.C
    src/error/fieldErrorVolume/fieldErrorsVolume.C
    src/hydrodynamics/hydrodynamicFunctionObject/hydrodynamicFunctionObject.C
    src/hydrodynamics/oscillatingDropletFunctionObject/oscillatingDropletFunctionObject.C
    src/hydrodynamics/risingBubbleFunctionObject/risingBubbleFunctionObject.C
    src/hydrodynamics/stationaryDropletFunctionObject/stationaryDropletFunctionObject.C
)
target_include_directories("twoPhaseValidation" PUBLIC
    src/divFree/divFreeFunctionObject
    src/divFree/fieldModels/deformation2DFieldModel
    src/divFree/fieldModels/deformationFieldModel
    src/divFree/fieldModels/divFreeFieldModel
    src/divFree/fieldModels/harmonicFieldModel
    src/divFree/fieldModels/periodicFieldModel
    src/divFree/fieldModels/rotationFieldModel
    src/divFree/fieldModels/shear2DFieldModel
    src/divFree/fieldModels/shearFieldModel
    src/divFree/fieldModels/supermanFieldModel
    src/divFree/fieldModels/translationFieldModel
    src/error/fieldError
    src/error/fieldErrorBoundedness
    src/error/fieldErrorComposite
    src/error/fieldErrorFunctionObject
    src/error/fieldErrorInterfaceSharpness
    src/error/fieldErrorInterfaceSharpness2
    src/error/fieldErrorInterfaceSharpness3
    src/error/fieldErrorL1
    src/error/fieldErrorL1normalized
    src/error/fieldErrorVolume
    src/error/testing
    src/hydrodynamics/hydrodynamicFunctionObject
    src/hydrodynamics/oscillatingDropletFunctionObject
    src/hydrodynamics/risingBubbleFunctionObject
    src/hydrodynamics/stationaryDropletFunctionObject
    src/meshMagnitudeTemplate
)
# Using PUBLIC here removes the need to specify again the required include directories
# for the applications.
target_include_directories("twoPhaseValidation" SYSTEM PUBLIC
    ${of_src_path}/finiteVolume/lnInclude
    ${of_src_path}/meshTools/lnInclude
    ${of_src_path}/surfMesh/lnInclude
    ${of_src_path}/OSspecific/POSIX/lnInclude
    ${of_src_path}/OpenFOAM/lnInclude
)

target_link_libraries("twoPhaseValidation"
    ${OF_DEPS}
)

# Install the library
install(TARGETS "twoPhaseValidation" LIBRARY)



#------------------------------------------------------------------------------
#   Executables
#------------------------------------------------------------------------------
set(OF_SOLVER_INCLUDES 
    applications/solvers
    ${of_src_path}/transportModels/twoPhaseMixture/lnInclude
    ${of_src_path}/transportModels
    ${of_src_path}/transportModels/incompressible/lnInclude
    ${of_src_path}/transportModels/interfaceProperties/lnInclude
    ${of_src_path}/TurbulenceModels/turbulenceModels/lnInclude
    ${of_src_path}/TurbulenceModels/incompressible/lnInclude
    ${of_src_path}/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude
    ${of_src_path}/fvOptions/lnInclude
)

# Testing
add_executable("testFieldError"
    applications/tests/testFieldError/testFieldError.C
)
target_link_libraries("testFieldError"
    "twoPhaseValidation"
    ${OF_DEPS}
)

add_executable("testFieldErrorComposite"
    applications/tests/testFieldError/testFieldError.C
)
target_link_libraries("testFieldErrorComposite"
    "twoPhaseValidation"
    ${OF_DEPS}
)

# Postprocessing
add_executable("calcBubbleData"
    applications/utilities/postProcessing/calcBubbleData/calcBubbleData.C
)
target_include_directories("calcBubbleData" SYSTEM PRIVATE
    ${of_src_path}/finiteVolume/lnInclude
    ${of_src_path}/meshTools/lnInclude
    ${of_src_path}/OSspecific/POSIX/lnInclude
    ${of_src_path}/OpenFOAM/lnInclude
)
target_link_libraries("calcBubbleData"
    ${OF_DEPS}
)

add_executable("calcVolFieldErrors"
    applications/utilities/postProcessing/calcVolFieldErrors/calcVolFieldErrors.C
)
target_link_libraries("calcVolFieldErrors"
    "twoPhaseValidation"
    ${OF_DEPS}
)

# Install executables
install(TARGETS
    # Testing
    "testFieldError"
    "testFieldErrorComposite"

    # Postprocessing
    "calcBubbleData"
    "calcVolFieldErrors"

    RUNTIME
)
