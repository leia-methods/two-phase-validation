#!/bin/bash

currPath =$PWD

for file in $(find . -name 'Allrun')
do
    echo $file
    $file
done

# not necessary anymore: postProcessing already executed by the Allrun-scripts
#for file in $(find . -name 'constant')
#do
#    cd $file
#    cd ..
#    calcVolFieldErrors -field alpha1 -allErrors
#    cd $currPath
#done
