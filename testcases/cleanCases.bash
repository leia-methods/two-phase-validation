#!/bin/sh
cd ${0%/*} || exit 1    # run from this directory

# Source tutorial clean functions
. $WM_PROJECT_DIR/bin/tools/CleanFunctions

currPath=$PWD

for file in $(find . -name 'constant')
do
    echo $file
    cd $file
    cd ..
#    cleanCase
#    rm alpha1Errors.dat
    ./Allclean
    cd $currPath
done

