# Two Phase Validation Library

This library consists of function objects used to explicitly set

* the cell-centered velocity field
* the volumetric flux field (face centered)
* the point centered velocity field

using explicit functions that define divergence-free velocity fields at any evaluated
point in space. These functions define standard test cases used for validating the 
interface motion algorithms of DNS methods for two-phase flows. 

The function object library is completely decoupled from the solver which means
it can be used together with any solver (implementing an interface advection
algorithm) without modifying the code of the solver. 

Author: 
    Tomislav Maric (maric@mma.tu-darmstadt.de)

Contributors: 
    Daniel Deising (deising@csi.tu-darmstadt.de, daniel.deising@rwth-aachen.de)
    Tobias Tolle (tolle@mma.tu-darmstadt.de)

### Usage 

* validating the geometrical volume fraction transport algorithm 
* validating algebraic advection schemes for the marker field
* validating the advection algorithm for the fluid front used in the front-tracking method
or its derivatives
* validating the advection of the interface for the level set method

### Measurable advection errors 

* Volume conservation error 
* Numerical boundedness error
* L1 geometric error 
* L1 geometric error normalized

To get all advection errors use to utility 'calcVolFieldErrors'
after the testcase has finished:

    calcVolFieldErrors -field advectedFieldName -allErrors

### Dependencies 
* OpenFOAM-plus v1912
* CMake 3.13 or higher
* Gcc 8 or higher (tested with 9.2.1)

## Compilation and installation
`two-phase-validation` is using the [CMake](https://cmake.org) build system.
Make sure that you have sourced OpenFOAM's `bashrc` so that the OpenFOAM environment variables are set properly.
Then execute the following commands inside the `two-phase-validation` directory:
    
    mkdir build && cd build
    cmake -DCMAKE_INSTALL_PREFIX=./ -DCMAKE_BUILD_TYPE=Release -DCMAKE_EXPORT_COMPILE_COMMANDS=on ..
    make && make install

This will configure, compile and install the `two-phase-validation` library
and its applications to `$FOAM_USER_LIBBIN` and `$FOAM_USER_APPBIN`, respectively.
Valid build types are `Release`, `Debug` and `RelWithDebInfo`. The flag
`-DCMAKE_EXPORT_COMPILE_COMMANDS=on` is optional.

When the library is compiled, you can use the
interface motion validating function objects as other function objects in
OpenFOAM. For example, to test the harmonic 2D shear advection test case 
with the MULES advection algorithm used in the interFoam solver, edit the 
`system/controlDict` file in the simulation case directory, and insert 
the following at the end of the file:     

    functions
    {
        advectionValidation 
        {
            type divFree; 
            divFree
            { 
                type shear; 
                // More parameters.
            }
        }
    }

    libs ("libtwoPhaseValidation.so"); 

#### Function object usage

First specify the type, which can be 'harmonic', 'periodic' or any of the baseTypes.
For a list of valid functions - as always in OpenFOAM - just enter an invalid name
and run the solver.

Key-entry 'harmonic':

* multiplies the velocity field by cos(pi*t/T)
* used to revert a transported field back to its initial position within time period T
* harmonic needs the following additional entries:

    period     <value>; // specifies the time period T

    phaseShift <value>; // multiply the velocity field by cos(pi*t/T + phi); phi = phaseShift

Key-entry 'periodic':

* switches the sign of the velocity field after t=T/2
* used to revert a transported field back to its initial position within time period T
* periodic needs the following additional entry:

    period <value>; // specifies the time period T

The baseType defines the testcase to be used, whose velocity fields are taken from literature
(see Section 'Test Cases'). Currently available test cases which correspond to the respective
keyword entry are:

* translation: specify a constant velocity field by adding entry: 

    V (Vx Vy Vz);

* rotation: add point on rotation axis: 

    axisPoint (x y z);

    additionaly add entry 'V' to specify the angular velocity at unit distance from
    the axisPoint, e.g. V (0 0 1); (to rotate around z-axis)

* deformation: 3D deformation case; no additional entries required
* deformation2d: 2D deformation case; no additional entries required
* shear: 3D shear case; no additional entries required
* shear2d: 2D shear case; no additional entries required

To specify the velocity fields at points, faces, cells, add the entries:

    cellVelocityName fieldName; // commonly 'U'
    volFluxName fieldName;      // commonly 'phi'
    pointVelocityName fieldName;

respectively.

### Test Cases

When using a solver from the interFoam family, make sure to set in fvSolution
the following entries to disable the PISO or PIMPLE loop:

    nCorrectors        -1;
    momentumPredictor  no;

This allows for testing only the advection algorithm without modifying the solver.

The case setups are taken from several publications:

* translation:
* rotation:
* deformation:
* deformation2d:
* shear:
* shear2d:

All test cases are set-up so that the transported field ideally reverts back to its original position and shape.
L1 errors are then computed based on the difference between the initial shape and the final shape of
the transported field.
